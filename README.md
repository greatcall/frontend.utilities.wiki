# FrontEnd.Utilities
Snippets of javascript and css to be used globally

### How do I install it?
`install-package GreatCall.FrontEnd.Utilities`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/frontend.utilities.wiki/wiki).
